set SERVEROUTPUT ON;
declare
cursor rollup2(nazwa_miasta varchar2) is 
  SELECT  m.nazwa as Miasto, f.rezyser, sum(s.ilosc) as ilosc_sprzed
  from miasto m
  JOIN kino k
  ON k.id_miasta = m.ID_MIASTA
  JOIN sprzedaz s
  ON s.id_kina = k.id_kina
  join film f
  on s.id_filmu = f.id_film
  where m.nazwa = nazwa_miasta
  GROUP BY rollup((m.nazwa,f.rezyser))
  order by m.nazwa asc ,(sum(s.ilosc)) desc;


begin 
for item in rollup2('Opole') loop
  dbms_output.put_line(item.miasto || ' ' || item.rezyser || ' ' || item.ilosc_sprzed);
end loop;
end;
/