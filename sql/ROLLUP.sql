-- Ranking sprzedaży w zależności od miasta/województa

SELECT m.nazwa Miasto, w.nazwa Wojewodztwo, sum(s.wartosc) Zarobki
FROM miasto m
JOIN wojewodztwo w
ON m.id_wojewodztwa = w.id_wojewodztwa
JOIN kino k
ON k.id_miasta = m.id_miasta
JOIN sprzedaz s
ON s.id_kina = k.id_kina
GROUP BY ROLLUP(w.nazwa, m.nazwa)
ORDER BY w.nazwa, m.nazwa;
