-- wyswietla dla ilosc kupionych biletow na dany film oraz padsumowanie dla calego gatunku
SELECT  f.tytul,g.nazwa as gatunek, sum(s.ilosc) as suma
FROM film f
JOIN gatunek g
ON f.id_gatunek = g.id_gatunek
JOIN sprzedaz s
ON s.id_filmu = f.id_film 
GROUP BY GROUPING SETS (g.nazwa,f.tytul),(g.nazwa)
;