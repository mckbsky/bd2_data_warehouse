SELECT  d.rok,d.miesiac,m.nazwa, sum(s.wartosc * s.ilosc) as zarobki,
sum(s.wartosc * s.ilosc) - LAG(sum(s.wartosc * s.ilosc), 1, 0) OVER (Partition by m.nazwa ORDER BY d.rok,d.miesiac) AS zarobki_saldo
from sprzedaz s
join Data d
on d.id_daty = s.id_daty
join Kino k
on s.id_kina = k.id_kina
join Miasto m
on m.id_miasta = k.id_miasta
group by d.rok,d.miesiac,m.nazwa
;