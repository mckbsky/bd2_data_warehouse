--  w którym miesiącu największa sprzedaż

SELECT k.adres, m.nazwa as Miasto, d.rok, d.miesiac, sum(s.wartosc) Zarobki
FROM miasto m
JOIN kino k
ON k.id_miasta = m.ID_MIASTA
JOIN sprzedaz s
ON s.id_kina = k.id_kina
join data d
on s.id_daty = d.id_daty
GROUP BY CUBE((k.adres,m.nazwa), (d.rok, d.miesiac))
ORDER BY k.adres asc, d.rok asc, d.miesiac asc;