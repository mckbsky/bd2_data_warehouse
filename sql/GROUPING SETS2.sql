-- ilosc sprzedanych bilet�w: ranking godzin w danym miescie oraz ogolnie godziny

SELECT  m.nazwa as Miasto, g.godzina, g.minuta, sum(s.ilosc) as ilosc_sprzed
FROM miasto m
JOIN kino k
ON k.id_miasta = m.ID_MIASTA
JOIN sprzedaz s
ON s.id_kina = k.id_kina
join data d
on s.id_daty = d.id_daty
JOIN Godzina g
on g.id_godz = d.id_godziny
GROUP BY grouping sets((g.godzina,g.minuta,m.nazwa),(g.godzina,g.minuta))
order by m.nazwa asc ,(sum(s.ilosc)) desc
;
