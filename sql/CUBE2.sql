select m.nazwa, to_char(to_date(lpad(to_char(d.dzien),2,'0')||lpad(to_char(d.miesiac),2,'0')||d.rok,'ddmmyyyy'),'DAY'), sum(s.ilosc)
from sprzedaz s
join kino k
on s.id_kina = k.id_kina
join miasto m
on m.id_miasta = k.id_miasta
join data d
on d.id_daty = s.id_daty
group by cube((m.nazwa),(to_char(to_date(lpad(to_char(d.dzien),2,'0')||lpad(to_char(d.miesiac),2,'0')||d.rok,'ddmmyyyy'),'DAY')))
order by m.nazwa asc, sum(s.ilosc) desc 
;