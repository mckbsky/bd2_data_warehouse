SELECT f.wytwornia,m.nazwa Nazwa_Miasta, ilosc, Sum(ilosc) over (PARTITION BY wytwornia ORDER BY
m.nazwa RANGE BETWEEN unbounded preceding AND CURRENT ROW) AS
suma from sprzedaz s
join film f
on f.ID_FILM = s.ID_FILMU
join kino k
on k.ID_KINA = s.ID_KINA
join miasto m
on m.ID_MIASTA= k.ID_MIASTA;