select s.ID_KINA, k.adres, m.nazwa as Miasto, sum(s.ilosc*s.wartosc) as zarobione,
RANK() OVER ( ORDER BY SUM(s.ilosc*s.wartosc) DESC ) AS ranking
from sprzedaz s
join KINO k
on k.ID_KINA = s.ID_KINA
join miasto m
on m.ID_MIASTA = k.ID_MIASTA
group by s.ID_KINA, k.adres, m.nazwa
order by ranking,s.id_kina;