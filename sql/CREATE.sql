DROP TABLE Godzina CASCADE CONSTRAINTS;
DROP TABLE Data CASCADE CONSTRAINTS;
DROP TABLE Gatunek CASCADE CONSTRAINTS;
DROP TABLE Kraj CASCADE CONSTRAINTS;
DROP TABLE Film CASCADE CONSTRAINTS;
DROP TABLE Wojewodztwo CASCADE CONSTRAINTS;
DROP TABLE Miasto CASCADE CONSTRAINTS;
DROP TABLE Kino CASCADE CONSTRAINTS;
DROP TABLE Klient CASCADE CONSTRAINTS;
DROP TABLE Miejsce CASCADE CONSTRAINTS;
DROP TABLE Zaplata CASCADE CONSTRAINTS;
DROP TABLE Sprzedaz CASCADE CONSTRAINTS;

CREATE TABLE Godzina (
	id_godz NUMBER(4) CONSTRAINT godz_pk PRIMARY KEY,
	godzina NUMBER(2),
	minuta NUMBER(2)
);

CREATE TABLE Data (
	id_daty NUMBER(10) CONSTRAINT data_pk PRIMARY KEY,
	dzien NUMBER(2),
	miesiac NUMBER(2),
	rok NUMBER(4),
	id_godziny NUMBER(5) CONSTRAINT data_idgodz_fk REFERENCES godzina(id_godz)
);

CREATE TABLE Gatunek (
	id_gatunek NUMBER(4) CONSTRAINT gatunek_pk PRIMARY KEY,
	nazwa VARCHAR2(20)
);

CREATE TABLE Kraj (
	id_kraj NUMBER(3) CONSTRAINT kraj_pk PRIMARY KEY,
	nazwa VARCHAR2(25)
);

CREATE TABLE Film (
	id_film NUMBER(4) CONSTRAINT film_pk PRIMARY KEY,
	tytul VARCHAR2(45),
	rezyser VARCHAR2(20),
	wytwornia VARCHAR2(20),
	rok_produkcji NUMBER(4),
	id_gatunek NUMBER(4) CONSTRAINT film_gatunek_fk REFERENCES gatunek(id_gatunek),
	id_kraj_produkcji NUMBER(3) CONSTRAINT film_kraj_fk REFERENCES kraj(id_kraj)
);

CREATE TABLE Wojewodztwo (
	id_wojewodztwa NUMBER(2) CONSTRAINT wojewodztwo_pk PRIMARY KEY,
	nazwa VARCHAR2(20)
);

CREATE TABLE Miasto (
	id_miasta NUMBER(5) CONSTRAINT miasto_pk PRIMARY KEY,
	nazwa VARCHAR2(40),
	id_wojewodztwa NUMBER(2) CONSTRAINT miasto_woj_fk REFERENCES wojewodztwo(id_wojewodztwa)
);

CREATE TABLE Kino (
	id_kina NUMBER(4) CONSTRAINT kino_pk PRIMARY KEY,
	adres VARCHAR2(45),
	id_miasta NUMBER(5) CONSTRAINT kino_miasto_fk REFERENCES miasto(id_miasta)
);

CREATE TABLE Klient (
	id_klienta NUMBER(4) CONSTRAINT klient_pk PRIMARY KEY,
	imie VARCHAR2(20),
	nazwisko VARCHAR2(20),
	nr_tel NUMBER(15)
);

CREATE TABLE Miejsce (
	id_miejsca NUMBER(6) CONSTRAINT miejsce_pk PRIMARY KEY,
	rzad NUMBER(3),
	kolumna NUMBER(3)
);

CREATE TABLE Zaplata  (
	id_zaplaty NUMBER(1) CONSTRAINT zaplata_pk PRIMARY KEY,
	nazwa VARCHAR2(20)
);

CREATE TABLE Sprzedaz (
	id_sprzedazy NUMBER(6) CONSTRAINT sprzedaz_pk PRIMARY KEY,
	ilosc NUMBER(3),
	wartosc NUMBER(3),
	id_kina NUMBER(4) CONSTRAINT sprzedaz_kino_fk REFERENCES kino(id_kina),
	id_daty NUMBER(10) CONSTRAINT sprzedaz_data_fk REFERENCES data(id_daty),
	id_filmu NUMBER(4) CONSTRAINT sprzedaz_film_fk REFERENCES film(id_film),
	id_miejsca NUMBER(5) CONSTRAINT sprzedaz_miejsce_fk REFERENCES miejsce(id_miejsca),
	id_zaplaty NUMBER(5) CONSTRAINT sprzedaz_zaplata_fk REFERENCES zaplata(id_zaplaty),
	id_klienta NUMBER(4) CONSTRAINT sprzedarz_klient_fk REFERENCES klient(id_klienta)
);
